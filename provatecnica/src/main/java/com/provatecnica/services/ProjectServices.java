package com.provatecnica.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.provatecnica.model.Log;
import com.provatecnica.model.Project;
import com.provatecnica.repository.LogRepository;
import com.provatecnica.repository.ProjectRepository;
import com.provatecnica.services.exception.ProjectNaoEncontradoExcpetion;
import com.provatecnica.services.exception.ProjetoExistenteException;

@Service
public class ProjectServices {

	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private LogRepository logRepository;
	
	public List<Project> listar(){
		logRepository.save(new Log("listar", "/projeto/listar", "", "GET"));
		return projectRepository.findAll();
	}
	
	public Project buscar(Long id) {
		Project employee = projectRepository.findOne(id);
		
		if(employee == null) {
			logRepository.save(new Log("buscar", "/projeto/"+id, "O Projeto não pode ser encontrado!", "GET"));
			throw new ProjectNaoEncontradoExcpetion("O Projeto não pode ser encontrado!");
		}else {
			logRepository.save(new Log("buscar", "/projeto/"+id, "", "GET"));
		}
		
		return employee;
	}
	
	public Project salvar(Project projeto) {
			Project p = projectRepository.buscarPorNome(projeto.getName());
			if(p != null) {
				logRepository.save(new Log("salvar", "/projeto", "Este nome de projeto já existe.", "POST"));
				throw new ProjetoExistenteException("Este nome de projeto já existe.");
			}else {
				logRepository.save(new Log("salvar", "/projeto", "", "POST"));
			}
		return projectRepository.save(projeto);
	}
	
	public void deletar(Long id) {
		try{
			logRepository.save(new Log("deletar", "/projeto/"+id, "", "DELETE"));
			projectRepository.delete(id);
		}catch (EmptyResultDataAccessException e) {
			logRepository.save(new Log("deletar", "/projeto/"+id, "O Projeto não pode ser encontrado.", "DELETE"));
			throw new ProjectNaoEncontradoExcpetion("O Projeto não pode ser encontrado.");
		}
	}
	
	public void atualizar(Project employee) {
		verificarExistencia(employee);
		logRepository.save(new Log("atualizar", "/projeto/"+employee.getId(), "", "PUT"));
		projectRepository.save(employee);
	}
	
	private void verificarExistencia(Project employee) {
		buscar(employee.getId());
	}
}
 