package com.provatecnica.services.exception;

public class EmployeeLimiteProjetoException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public EmployeeLimiteProjetoException(String mensagem) {
		super(mensagem);
	}
	
	public EmployeeLimiteProjetoException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
	
}
