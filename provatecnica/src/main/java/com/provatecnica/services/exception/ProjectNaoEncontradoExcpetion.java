package com.provatecnica.services.exception;

public class ProjectNaoEncontradoExcpetion extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public ProjectNaoEncontradoExcpetion(String mensagem) {
		super(mensagem);
	}
	
	public ProjectNaoEncontradoExcpetion(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
	
}
