package com.provatecnica.services.exception;

public class EmployeeJaContemOProjetoExcpetion extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public EmployeeJaContemOProjetoExcpetion(String mensagem) {
		super(mensagem);
	}
	
	public EmployeeJaContemOProjetoExcpetion(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
	
}
