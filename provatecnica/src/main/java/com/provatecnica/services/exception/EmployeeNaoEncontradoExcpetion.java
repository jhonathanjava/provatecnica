package com.provatecnica.services.exception;

public class EmployeeNaoEncontradoExcpetion extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public EmployeeNaoEncontradoExcpetion(String mensagem) {
		super(mensagem);
	}
	
	public EmployeeNaoEncontradoExcpetion(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
	
}
