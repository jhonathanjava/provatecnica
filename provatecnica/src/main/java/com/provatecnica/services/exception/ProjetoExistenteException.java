package com.provatecnica.services.exception;

public class ProjetoExistenteException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public ProjetoExistenteException(String mensagem) {
		super(mensagem);
	}
	
	public ProjetoExistenteException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
	
}
