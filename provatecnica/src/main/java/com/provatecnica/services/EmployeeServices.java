package com.provatecnica.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.provatecnica.model.Employee;
import com.provatecnica.model.Log;
import com.provatecnica.model.Project;
import com.provatecnica.repository.EmployeeRepository;
import com.provatecnica.repository.LogRepository;
import com.provatecnica.repository.ProjectRepository;
import com.provatecnica.services.exception.EmployeeJaContemOProjetoExcpetion;
import com.provatecnica.services.exception.EmployeeLimiteProjetoException;
import com.provatecnica.services.exception.EmployeeNaoEncontradoExcpetion;

@Service
public class EmployeeServices {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private LogRepository logRepository;
	
	public List<Employee> listar(){
		logRepository.save(new Log("listar", "/funcionario/listar", "", "GET"));
		return employeeRepository.findAll();
	}
	
	public Employee buscar(Long id) {
		Employee employee = employeeRepository.findOne(id);
		
		if(employee == null) {
			logRepository.save(new Log("buscar", "/funcionario/"+id, "O Funcionario não pode ser encontrado!", "GET"));
			throw new EmployeeNaoEncontradoExcpetion("O Funcionario não pode ser encontrado!");
		}else {
			logRepository.save(new Log("buscar", "/funcionario/"+id, "", "GET"));
		}
		
		return employee;
	}
	
	public Employee salvar(Employee employee) {
		logRepository.save(new Log("buscar", "/funcionario/salvar", "", "POST"));
		employee.setId(null);
		System.out.println(employee);
		System.out.println("Tensand"+employeeRepository.save(employee));
		return employeeRepository.save(employee);
	}
	
	public void deletar(Long id) {
		try{
			employeeRepository.delete(id);
			logRepository.save(new Log("deletar", "/funcionario/"+id, "", "DELETE"));
		}catch (EmptyResultDataAccessException e) {
			logRepository.save(new Log("deletar", "/funcionario/"+id, "", "DELETE"));
			throw new EmployeeNaoEncontradoExcpetion("O Funcionario não pode ser encontrado.");
		}
	}
	
	public void atualizar(Employee employee) {
		verificarExistencia(employee);
		logRepository.save(new Log("atualizar", "/funcionario/"+employee.getId(), "", "PUT"));
		employeeRepository.save(employee);
	}
	
	private void verificarExistencia(Employee employee) {
		buscar(employee.getId());
	}
	
	public Project salvarProjeto(Long funcionarioId, Project projeto) {
		logRepository.save(new Log("adicionaProjeto", "/funcionario/"+funcionarioId+"/projetos", "", "POST"));
		Employee employee = buscar(funcionarioId);
		Project p = new Project();
		if(projeto.getId() != null) {
			logRepository.save(new Log("buscaPorId", "/funcionario/"+funcionarioId+"/projetos", "", "POST"));
			projeto = projectRepository.findOne(projeto.getId());
		}else {
			logRepository.save(new Log("buscaPorNome", "/funcionario/"+funcionarioId+"/projetos", "", "POST"));
			p = projectRepository.buscarPorNome(projeto.getName());
			if(p != null && p.getId() != null) {
				projeto = p;
			}
			logRepository.save(new Log("salvaProjeto", "/funcionario/"+funcionarioId+"/projetos", "", "POST"));
			projeto = projectRepository.save(projeto);
		}
		if(verificaSeContemOProjeto(employee,projeto)) {
			if(verificaLimiteProjetoDoFuncionario(employee,projeto)) {
				logRepository.save(new Log("associaProjetoAoFuncionario", "/funcionario/"+funcionarioId+"/projetos", "", "POST"));
				employee.adicionarProjeto(projeto);
				employeeRepository.save(employee);
			}else {
				logRepository.save(new Log("buscaPorId", "/funcionario/"+funcionarioId+"/projetos", "Esse Funcionário já contém 2 projeto.", "POST"));
				throw new EmployeeLimiteProjetoException("Esse Funcionário já contém 2 projeto.");
			}
		}else {
			logRepository.save(new Log("buscaPorId", "/funcionario/"+funcionarioId+"/projetos", "Esse Funcionário já contém este projeto.", "POST"));
			throw new EmployeeJaContemOProjetoExcpetion("Esse Funcionário já contém este projeto.");
		}
		return projeto;
	}
	
	private boolean verificaLimiteProjetoDoFuncionario(Employee emp, Project projeto) {
		if(emp.getProjetos().size() == 2) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	private boolean verificaSeContemOProjeto(Employee emp, Project projeto) {
		if(emp.getProjetos().contains(projeto)) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	public List<Employee> listarFuncionarioMaisDeUmProjeto(){
		logRepository.save(new Log("listarFuncionarioMaisDeUmProjeto", "/funcionario/listarFuncionarioMaisDeUmProjeto", "", "GET"));
		List<Employee> temp = employeeRepository.listarFuncionarioMaisDeUmProjeto();
		List<Employee> retorno = new ArrayList<>();
		temp.forEach(item -> {
			if(item.getProjetos() != null && item.getProjetos().size() > 1) {
				retorno.add(item);
			}
		});
		return retorno;
	}

	public EmployeeRepository getEmployeeRepository() {
		return employeeRepository;
	}

	public void setEmployeeRepository(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	public ProjectRepository getProjectRepository() {
		return projectRepository;
	}

	public void setProjectRepository(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}

	public LogRepository getLogRepository() {
		return logRepository;
	}

	public void setLogRepository(LogRepository logRepository) {
		this.logRepository = logRepository;
	}
	
	
}
 