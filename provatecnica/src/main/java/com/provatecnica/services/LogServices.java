package com.provatecnica.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.provatecnica.model.Log;
import com.provatecnica.repository.LogRepository;

@Service
public class LogServices {

	@Autowired
	private LogRepository logRepository;
	
	public void salvar(Log log) {
		logRepository.save(log);
	}
	
}
 