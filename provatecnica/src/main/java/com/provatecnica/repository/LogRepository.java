package com.provatecnica.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.provatecnica.model.Log;

@Repository
public interface LogRepository extends JpaRepository<Log, Long>{

}
