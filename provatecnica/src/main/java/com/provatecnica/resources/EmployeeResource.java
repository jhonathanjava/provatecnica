package com.provatecnica.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.provatecnica.model.Employee;
import com.provatecnica.model.Project;
import com.provatecnica.services.EmployeeServices;

@RestController
@RequestMapping("/funcionario")
public class EmployeeResource {

	@Autowired
	private EmployeeServices employeeServices;

	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> listar() {
		return ResponseEntity.status(HttpStatus.OK).body(employeeServices.listar());
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> salvar(@Valid @RequestBody Employee employee) {
		employee = employeeServices.salvar(employee);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(employee.getId())
				.toUri();

		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Long id) {
		Employee employee = employeeServices.buscar(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(employee);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletar(@PathVariable("id") Long id) {
		employeeServices.deletar(id);
		return ResponseEntity.noContent().build();

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@RequestBody Employee employee, @PathVariable("id") Long id) {
		employee.setId(id);
		employeeServices.atualizar(employee);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value = "/listarFuncionarioMaisDeUmProjeto", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> listarFuncionarioMaisDeUmProjeto(){
		List<Employee> funcionarios = employeeServices.listarFuncionarioMaisDeUmProjeto();
		return ResponseEntity.status(HttpStatus.OK).body(funcionarios);
	}
	
	@RequestMapping(value = "/{id}/projetos", method = RequestMethod.POST)
	public ResponseEntity<Void> adicionaProjeto( @PathVariable("id") Long funcionarioId,
			@Valid @RequestBody Project projeto) {
		
		employeeServices.salvarProjeto(funcionarioId, projeto);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		
		return ResponseEntity.created(uri).build();
	}

}
