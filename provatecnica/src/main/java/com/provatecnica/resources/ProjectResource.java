package com.provatecnica.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.provatecnica.model.Project;
import com.provatecnica.services.ProjectServices;

@RestController
@RequestMapping("/projeto")
public class ProjectResource {

	@Autowired
	private ProjectServices projectServices;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Project>> listar() {
		return ResponseEntity.status(HttpStatus.OK).body(projectServices.listar());
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> salvar(@Valid @RequestBody Project project) {
		project = projectServices.salvar(project);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(project.getId())
				.toUri();

		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Long id) {
		Project project = projectServices.buscar(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(project);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletar(@PathVariable("id") Long id) {
		projectServices.deletar(id);
		return ResponseEntity.noContent().build();

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@RequestBody Project project, @PathVariable("id") Long id) {
		project.setId(id);
		projectServices.atualizar(project);
		return ResponseEntity.noContent().build();
	}
}
