package com.provatecnica.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.provatecnica.services.exception.EmployeeLimiteProjetoException;;

@Entity
@Table(name ="employee")
@SequenceGenerator(name="seq_employee", sequenceName="seq_employee", initialValue=1, allocationSize=1)
public class Employee {

	/*
	 * @JsonInclude: Retorna a informação apenas se não for nula
	 */
	@JsonInclude(Include.NON_NULL)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_employee")
	@org.hibernate.annotations.GenericGenerator(
			name="seq_employee", 
			strategy = "sequence-identity",
			parameters = @org.hibernate.annotations.Parameter(value = "seq_employee", name = "sequence")
	)
	private Long id;
	
	@NotEmpty(message = "O campo name não pode ser vazio")
	@Size(min = 2, max = 300, message = "O Campo name deve ter no mínimo {min} caracteres no máximo {max} caracteres")
	private String name; 
	
	private BigDecimal salary;
	
	@JsonInclude(Include.NON_NULL)
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="emp_proj", joinColumns= {@JoinColumn(name="emp_id")}, inverseJoinColumns= {@JoinColumn(name="proj_id")})
	private List<Project> projetos = new ArrayList<>();
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(String name, BigDecimal salary) {
		this.name = name;
		this.salary = salary;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getSalary() {
		return salary;
	}
	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}
	
	public List<Project> getProjetos() {
		return projetos;
	}
	
	public void setProjetos(List<Project> projetos) {
		this.projetos = projetos;
	}
	
	public void adicionarProjeto(Project projeto) {
		if(projetos.size() == 2) {
			throw new EmployeeLimiteProjetoException("Esse Funcionário já contém 2 projeto.");
		}else {
			projetos.add(projeto);
		}
	}
	
	public void removeProjeto(Project projeto) {
		projetos.remove(projeto);
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + ", projetos=" + projetos + "]";
	}
	
}
