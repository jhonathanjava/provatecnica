package com.provatecnica.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.GenericGenerator;


@Entity
@SequenceGenerator(name="seq_log", sequenceName="seq_log", initialValue=1, allocationSize=1)
public class Log {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_log")
	@org.hibernate.annotations.GenericGenerator(
			name="seq_log", 
			strategy = "sequence-identity",
			parameters = @org.hibernate.annotations.Parameter(value = "seq_log", name = "sequence")
	)
	private Long id;
	
	private String acao;
	
	private String location;
	
	private String exception;
	
	private String metodoRequisicao;
	
	public Log() {
		// TODO Auto-generated constructor stub
	}
	
	public Log(String acao, String location, String exception, String metodoRequisicao) {
		this.acao = acao;
		this.location = location;
		this.exception = exception;
		this.metodoRequisicao = metodoRequisicao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getMetodoRequisicao() {
		return metodoRequisicao;
	}
	
	public void setMetodoRequisicao(String metodoRequisicao) {
		this.metodoRequisicao = metodoRequisicao;
	}
}
