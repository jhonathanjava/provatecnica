package com.provatecnica.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@SequenceGenerator(name="seq_project", sequenceName="seq_project", initialValue=1, allocationSize=1)
public class Project {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_project")
	@org.hibernate.annotations.GenericGenerator(
			name="seq_project", 
			strategy = "sequence-identity",
			parameters = @org.hibernate.annotations.Parameter(value = "seq_project", name = "sequence")
	)
	private Long id;
	
	@NotEmpty(message = "O campo name não pode ser vazio")
	@Size(min = 2, max = 300, message = "O Campo name deve ter no mínimo {min} caracteres no máximo {max} caracteres")
	@Column(unique = true)
	private String name;
	
	public Project() {
		// TODO Auto-generated constructor stub
	}
	
	public Project(String name) {
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	} 
	
}
