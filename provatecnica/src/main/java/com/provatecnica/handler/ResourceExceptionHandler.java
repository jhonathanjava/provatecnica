package com.provatecnica.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.provatecnica.model.DetalhesErro;
import com.provatecnica.services.exception.EmployeeJaContemOProjetoExcpetion;
import com.provatecnica.services.exception.EmployeeLimiteProjetoException;
import com.provatecnica.services.exception.EmployeeNaoEncontradoExcpetion;
import com.provatecnica.services.exception.ProjectNaoEncontradoExcpetion;
import com.provatecnica.services.exception.ProjetoExistenteException;

@ControllerAdvice
public class ResourceExceptionHandler {

	@ExceptionHandler(EmployeeNaoEncontradoExcpetion.class)
	public ResponseEntity<DetalhesErro> handleEmployeeNaoEncontradoException(
			EmployeeNaoEncontradoExcpetion e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(404L);
		erro.setTitulo("O Funcionario não pode ser encontrado");
		erro.setMensagemDesenvolvedor("");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
	
	@ExceptionHandler(ProjectNaoEncontradoExcpetion.class)
	public ResponseEntity<DetalhesErro> handleProjetoNaoEncontradoException(
			ProjectNaoEncontradoExcpetion e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(404L);
		erro.setTitulo("O Projeto não pode ser encontrado");
		erro.setMensagemDesenvolvedor("");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
	
	@ExceptionHandler(ProjetoExistenteException.class)
	public ResponseEntity<DetalhesErro> handleProjetoExistenteException(
			ProjetoExistenteException e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(409L);
		erro.setTitulo("Este nome de projeto já existe.");
		erro.setMensagemDesenvolvedor("");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body(erro);
	}
	
	@ExceptionHandler(EmployeeJaContemOProjetoExcpetion.class)
	public ResponseEntity<DetalhesErro> handleEmployeeJaContemOProjetoExcpetion(
			EmployeeJaContemOProjetoExcpetion e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(409L);
		erro.setTitulo("Esse Funcionário já contém este projeto.");
		erro.setMensagemDesenvolvedor("");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body(erro);
	}
	@ExceptionHandler(EmployeeLimiteProjetoException.class)
	public ResponseEntity<DetalhesErro> handleEmployeeLimiteProjetoException(
			EmployeeLimiteProjetoException e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(409L);
		erro.setTitulo("Esse Funcionário já contém 2 projeto.");
		erro.setMensagemDesenvolvedor("");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body(erro);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<DetalhesErro> handleDataIntegrityViolationException(
			DataIntegrityViolationException e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(400L);
		erro.setTitulo("Requisição Inválida");
		erro.setMensagemDesenvolvedor("");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
	}
	
}
