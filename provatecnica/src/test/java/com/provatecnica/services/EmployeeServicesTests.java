package com.provatecnica.services;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.provatecnica.model.Employee;
import com.provatecnica.model.Project;
import com.provatecnica.services.exception.EmployeeLimiteProjetoException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServicesTests {

	@Autowired
	EmployeeServices employeeServices;

	private Employee employee;

	private Project project;

	@Before
	public void before() {
		employee = new Employee("Funcionario1", new BigDecimal(9800.00));
		project = new Project("Projeto 1");
	}

	@Test
	public void salvaFuncionario() {
		employee = employeeServices.salvar(employee);
		Employee x = employeeServices.buscar(employee.getId());
		assertEquals(x.getId(), employee.getId());
	}

	@Test
	public void adicionaFuncionarioEmUmProjeto() {
		employee.adicionarProjeto(project);
		assertEquals(1, employee.getProjetos().size());
	}

	@Test
	public void adicionaFuncionarioEmDoisProjeto() {
		Project project2 = new Project("Projeto 2");
		employee.adicionarProjeto(project);
		employee.adicionarProjeto(project2);

		assertEquals(2, employee.getProjetos().size());
	}

	@Test(expected = EmployeeLimiteProjetoException.class)
	public void adicionaFuncionarioEmTresProjeto() throws Exception {
		Project project2 = new Project("Projeto 2");
		Project project3 = new Project("Projeto 3");
		employee.adicionarProjeto(project);
		employee.adicionarProjeto(project2);
		employee.adicionarProjeto(project3);
	}

	@Test
	public void listarFuncionarioMaisDeUmProjeto() {
		// Deve sobrar apenas esse no resultado final
		employee = employeeServices.salvar(employee);
		project = employeeServices.salvarProjeto(employee.getId(), project);
		
		Project project2 = new Project("Projeto 2");
		project2 = employeeServices.salvarProjeto(employee.getId(), project2);
		
		//Criado mais um funcionario mais no resultado final não sera considerado  pois possui 1 projeto apenas
		Employee tempF = new Employee("Funcionario 3", new BigDecimal(5980.00));
		tempF = employeeServices.salvar(tempF);
		employeeServices.salvarProjeto(tempF.getId(), project2);
		
		List<Employee> listarFuncionarioMaisDeUmProjeto = employeeServices.listarFuncionarioMaisDeUmProjeto();

		assertEquals(1, listarFuncionarioMaisDeUmProjeto.size());
	}
}
