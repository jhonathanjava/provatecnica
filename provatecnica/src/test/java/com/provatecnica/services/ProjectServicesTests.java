package com.provatecnica.services;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.provatecnica.model.Project;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectServicesTests {

	@Autowired
	ProjectServices projectServices;

	private Project project;

	@Before
	public void before() {
		project = new Project("Projeto 1");
	}

	@Test
	public void salvaFuncionario() {
		project = projectServices.salvar(project);
		Project p = projectServices.buscar(project.getId());
		assertEquals(p.getId(), project.getId());
	}

}
