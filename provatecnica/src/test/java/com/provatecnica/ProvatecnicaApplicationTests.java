package com.provatecnica;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.provatecnica.model.Employee;
import com.provatecnica.model.Project;
import com.provatecnica.repository.EmployeeRepository;
import com.provatecnica.services.exception.EmployeeLimiteProjetoException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProvatecnicaApplicationTests {

	@Autowired
	EmployeeRepository employeeServices;
	
	private Employee employee;

	private Project project;
	
	@Before
	public void before() {
		employee = new Employee("Funcionario1",new BigDecimal(9800.00));
	}

	@Test
	public void salvaFuncionario() {
		employee = employeeServices.save(employee);
		Employee x = employeeServices.findOne(employee.getId()); 
		assertEquals(x.getId(), employee.getId());
	}
	
	@Test
		public void adicionaFuncionarioEmUmProjeto() {
			employee.adicionarProjeto(project);
			assertEquals(1, employee.getProjetos().size());
		}
		
		@Test
		public void adicionaFuncionarioEmDoisProjeto() {
			Project project2 = new Project("Projeto 2");
			employee.adicionarProjeto(project);
			employee.adicionarProjeto(project2);
			
			assertEquals(2, employee.getProjetos().size());
		}
		
		@Test(expected = EmployeeLimiteProjetoException.class)
		public void adicionaFuncionarioEmTresProjeto() throws Exception{
			Project project2 = new Project("Projeto 2");
			Project project3 = new Project("Projeto 3");
			employee.adicionarProjeto(project);
			employee.adicionarProjeto(project2);
			employee.adicionarProjeto(project3);
		}
}
